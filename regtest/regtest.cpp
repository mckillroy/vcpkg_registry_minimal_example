#ifdef FEATURED_LIB
	#include "thefeaturelib/thefeaturelib.hpp"
#else
	#include "thelib/thelib.hpp"
#endif

int main(int argc, char** argv) {

#ifdef FEATURED_LIB
	thefeaturelibfunc();
#else
	thelibfunc();
#endif

	return 0;
}
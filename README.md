vcpkg registry test minimal case
--------------------------------------

To have this little example project work with Visual Studio as intented, please edit the field "cmakeCommandArgs" in 
CMakeSettings.json to point to your vcpkg.cmake toolchain file.

`/registry`	contains the local filesystem based example vcpkg registry which provides "thelib" library.

`/regtest`	contains the source code of a minimal example program.

`/the_lib`	contains a tiny library "thelib" used by the minimal example and provided by the registry.

For direct calls to cmake on Linux or Windows use 
`cmake -GNinja -S . -B build -DCMAKE_TOOLCHAIN_FILE=<location of your (VCPKG_ROOT)/scripts/buildsystems/vcpkg.cmake file>`